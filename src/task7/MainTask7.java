package task7;

import java.util.Scanner;

class MainTask7 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a positive number: ");
        int number = scanner.nextInt();

        int previous = 0;
        int current = 1;
        int newFib = 1;

        for (int i = 1; i < number; i++) {
            newFib = current + previous;
            previous = current;
            current = newFib;
        }

        System.out.println(String.format("Fib: %s", newFib));
    }
}
