package task5;

import shared.SDANumber;

import java.util.Scanner;

class MainTask5 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a positive number: ");
        int number = scanner.nextInt();

        for (int i = 2; i < number; i++) {
            var currentNumber = new SDANumber(i);
            if (currentNumber.isPrime()) {
                System.out.println(i);
            }
        }
    }
}
