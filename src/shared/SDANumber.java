package shared;

public class SDANumber {

    private final int value;

    public SDANumber(int value) {
        this.value = value;
    }

    public boolean isPrime() {
        for (int i = 2; i < value; i++) {
            if (value % i == 0) {
                return false;
            }
        }

        return true;
    }

    public boolean isDivisibleBy(int divisor) {
        return value % divisor == 0;
    }

    public double calculateHarmonicSeries() {
        int i = 1;
        double sum = 0;
        while (i <= value) {
            double element = 1.00 / i;
            sum += element;
            i++;
        }

        return sum;
    }
}